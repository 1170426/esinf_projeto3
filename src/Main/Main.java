package Main;

import BST_Package.BST_Extended;
import Utils.CommonSequence;
import Utils.Decoder;
import Utils.Encoder;
import Utils.FileManager;
import Utils.LetterTree;
import Utils.OrderMorseWords;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static BST_Extended bsTree = new BST_Extended();
    private static BST_Extended bsTreeLetters = new BST_Extended();

    private static boolean validOption = true;
    private static boolean endOption = false;

    private static boolean imported = false;
    private static boolean createdLetters = false;

    public static void main(String[] args) throws IOException {
        do {
            if (!validOption) {
                System.out.println("Insira uma opção válida.");
                System.out.println("");
            }
            menuDisplay();
            Scanner in = new Scanner(System.in);
            try {
                int num = Integer.parseInt(in.nextLine());
                menu(num);
                if (!endOption) {
                    pause();
                }
            } catch (NumberFormatException e) {
                System.out.println("");
                System.out.println("Insira um número.");
                System.out.println("");
            }
        } while (!endOption);
    }

    private static void menuDisplay() {
        String str = "Menu:"
                + "\n1 - Importar ficheiros;"
                + "\n2 - Descodificar frase/palavra em código morse;"
                + "\n3 - Construir uma nova Binary Search Tree de código morse só com letras;"
                + "\n4 - Codificar frase/palavra para código morse (usando a Binary Search Tree que só contém letras);"
                + "\n5 - Encontrar a sequência inicial de “pontos” e “traços” comum a dois símbolos (usando a Binary Search Tree original);"
                + "\n6 - Filtrar e ordenar uma lista de palavras em código morse por total de ocorrências de um tipo de classe de símbolos específico que a compõem;"
                + "\n7 - Filtrar e ordenar uma lista de palavras em código morse por total de ocorrências de todos os tipos de classe de símbolos que a compõem;"
                + "\n8 - Apresentar o Binary Search Tree original;"
                + "\n9 - Apresentar o Binary Search Tree só com letras;"
                + "\n0 - Cancelar."
                + "\nOpção: ";
        System.out.print(str);
    }

    private static void menu(int option) throws IOException {
        System.out.println("");
        switch (option) {
            case 1:
                try {
                    imported = FileManager.readFile(new File("morse_v3.csv"), bsTree);
                    if (imported) {
                        System.out.println("Os dados do ficheiro foram importados corretamente.");
                    } else {
                        System.out.println("Os dados do ficheiro não foram importados corretamente.");
                    }
                } catch (FileNotFoundException fne) {
                    imported = false;
                    System.out.println("O ficheiro não foi encontrado");
                }
                validOption = true;
                break;
            case 2:
                if (imported) {
                    Scanner in = new Scanner(System.in);
                    System.out.print("Palavra/frase em código morse: ");
                    String morseSentence = in.nextLine();
                    System.out.println("");
                    try {
                        System.out.println("Palavra/frase descodificada: " + Decoder.decodeMorseCode(bsTree, morseSentence));
                    } catch (NullPointerException e) {
                        System.out.println("Código morse inválido.");
                    }
                } else {
                    System.out.println("Os dados ainda não foram importados.");
                }
                validOption = true;
                break;
            case 3:
                if (imported) {
                    bsTreeLetters = LetterTree.createLetterTree(bsTree, bsTreeLetters);
                    System.out.println("A Binary Search Tree composta somente por letras foi criada corretamente.");
                    createdLetters = true;
                } else {
                    System.out.println("Os dados ainda não foram importados.");
                }
                validOption = true;
                break;
            case 4:
                if (createdLetters) {
                    System.out.print("Frase para ser encriptada: ");
                    Scanner in = new Scanner(System.in);
                    String encripted = Encoder.sentenceEncoder(in.nextLine(), bsTreeLetters);
                    System.out.println("");
                    if (encripted.length() > 0) {
                        System.out.println("Frase encriptada: " + encripted);
                    } else {
                        System.out.println("A frase inserida não é válida por não ser composta somente por letras.");
                    }
                } else {
                    System.out.println("A Binary Search Tree composta somente por letras ainda não foi criada.");
                }
                validOption = true;
                break;
            case 5:
                if (imported) {
                    System.out.println("Insira duas frases: ");
                    Scanner in = new Scanner(System.in);
                    System.out.print("Frase 1: ");
                    String frase1 = in.nextLine();
                    System.out.print("Frase 2: ");
                    String frase2 = in.nextLine();
                    String commonSequence = CommonSequence.biggestCommonSequence(frase1, frase2, bsTree);
                    System.out.println("");
                    if (commonSequence.length() == 0){
                        System.out.println("Estas duas frases não têm nenhuma sequência em comum.");
                    } else {
                        System.out.println("Sequência em comum destas duas frases: ");
                        System.out.println(commonSequence);
                    }
                } else {
                    System.out.println("Os dados ainda não foram importados.");
                }
                validOption = true;
                break;
            case 6:
                if (imported) {
                    boolean worked;
                    do {
                        List<String> morseWords = new ArrayList<>();
                        String finish = "fim";
                        String word;
                        do {
                            Scanner in = new Scanner(System.in);
                            System.out.println("Escreva «fim» para terminar.");
                            System.out.print("Palavra/frase em código morse número " + (morseWords.size() + 1) + ": ");
                            word = in.nextLine();
                            if (word.compareToIgnoreCase(finish) != 0 && word.length() > 0) {
                                morseWords.add(word);
                            }
                            System.out.println("");
                        } while (word.compareToIgnoreCase(finish) != 0);
                        Scanner in2 = new Scanner(System.in);
                        System.out.print("Tipo de classe de símbolo: ");
                        String type = in2.nextLine();
                        try {
                            List<String> list = OrderMorseWords.orderMorseWords(bsTree, morseWords, type);
                            System.out.println("Ordenação por classe " + type + ": " + list);
                            worked = true;
                        } catch (NullPointerException e) {
                            System.out.println("Uma das palavras em código morse inseridas não é válida. Tente novamente.");
                            worked = false;
                        }
                    } while (!worked);
                } else {
                    System.out.println("Os dados ainda não foram importados.");
                }
                validOption = true;
                break;
            case 7:
                if (imported) {
                    boolean worked;
                    do {
                        List<String> morseWords = new ArrayList<>();
                        String finish = "fim";
                        String word;
                        List<String> types = new ArrayList<>(Arrays.asList("Letter", "Non-English", "Number", "Punctuation"));
                        do {
                            Scanner in = new Scanner(System.in);
                            System.out.println("Escreva «fim» para terminar.");
                            System.out.print("Palavra/frase em código morse número " + (morseWords.size() + 1) + ": ");
                            word = in.nextLine();
                            if (word.compareToIgnoreCase(finish) != 0 && word.length() > 0) {
                                morseWords.add(word);
                            }
                            System.out.println("");
                        } while (word.compareToIgnoreCase(finish) != 0);
                        System.out.println("");
                        try {
                            for (String type : types) {
                                List<String> list = OrderMorseWords.orderMorseWords(bsTree, morseWords, type);
                                System.out.println("Ordenação por classe " + type + ": " + list);
                            }
                            worked = true;
                        } catch (NullPointerException e) {
                            System.out.println("Uma das palavras em código morse inseridas não é válida. Tente novamente.");
                            worked = false;
                        }
                    } while (!worked);
                } else {
                    System.out.println("Os dados ainda não foram importados.");
                }
                validOption = true;
                break;
            case 8:
                if (imported) {
                    System.out.println(bsTree);
                } else {
                    System.out.println("Os dados ainda não foram importados.");
                }
                validOption = true;
                break;
            case 9:
                if (createdLetters) {
                    System.out.println(bsTreeLetters);
                } else {
                    System.out.println("A Binary Search Tree composta somente por letras ainda não foi criada.");
                }
                validOption = true;
                break;
            case 0:
                validOption = true;
                endOption = true;
                break;
            default:
                validOption = false;
                break;
        }
    }

    private static void pause() {
        Scanner in = new Scanner(System.in);
        System.out.println("");
        String ENTER = "===Digite ENTER para avançar===";
        System.out.format(ENTER + "\n");
        in.nextLine();
    }

}
