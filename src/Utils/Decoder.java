package Utils;

import BST_Package.BST_Extended;
import Model.morseCharacter;

public class Decoder {

    private static final String WORD_DIVIDER = "/";
    private static final String CHAR_DIVIDER = " ";

    public static String decodeMorseCode(BST_Extended bsTree, String morse) {
        String decoded = "";
        String[] words = morse.trim().split(WORD_DIVIDER);
        for (String word : words) {
            String[] letters = word.trim().split(CHAR_DIVIDER);
            String finalWord = "";
            for (String letter : letters) {
                morseCharacter decodedLetter = bsTree.findElement(letter);
                finalWord += decodedLetter.getAlfanumeric();
            }
            decoded += finalWord + " ";
        }
        return decoded.substring(0, decoded.length() - 1);
    }
    
    
}
