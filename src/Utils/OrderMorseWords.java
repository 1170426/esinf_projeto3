package Utils;

import BST_Package.BST_Extended;
import Model.morseCharacter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class OrderMorseWords {

    private static final String WORD_DIVIDER = "/";
    private static final String CHAR_DIVIDER = " ";

    public static List<String> orderMorseWords(BST_Extended bsTree, List<String> morseWords, String type) {
        return orderedWordsByType(stringTypeCounter(bsTree, morseWords, type));
    }

    private static List<String> orderedWordsByType(Map<String, Integer> morseWordType) {
        int max = 0;
        List<String> finalList = new ArrayList<>();
        for (String str : morseWordType.keySet()) {
            if (max < morseWordType.get(str)) {
                max = morseWordType.get(str);
            }
        }

        for (int i = 0; i < max; i++) {
            for (String str : morseWordType.keySet()) {
                if (morseWordType.get(str) == max - i) {
                    finalList.add(str);
                }
            }
        }
        return finalList;
    }
    //Devolve um map que contém a todas as frases descodificadas e o número de vezes que o tipo pedido existe nessa frase

    private static Map<String, Integer> stringTypeCounter(BST_Extended bsTree, List<String> morseWords, String type) {
        Map<String, Integer> map = new LinkedHashMap<>();
        for (String morseWord : morseWords) {
            String decoded = "";
            int cont = 0;
            for (String word : morseWord.trim().split(WORD_DIVIDER)) {
                String finalWord = "";
                for (String letter : word.trim().split(CHAR_DIVIDER)) {
                    morseCharacter morseLetter = bsTree.findElement(letter);
                    if (morseLetter.getType().compareToIgnoreCase(type) == 0) {
                        cont++;
                    }
                    finalWord += morseLetter.getAlfanumeric();
                }
                decoded += finalWord + " ";
            }
            map.put(decoded.substring(0, decoded.length() - 1), cont);
        }
        return map;
    }

}
