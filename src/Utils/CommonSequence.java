package Utils;

import BST_Package.BST_Extended;

public class CommonSequence {

    public static String biggestCommonSequence(String alfanumeric1, String alfanumeric2, BST_Extended bsTree) {
        if (alfanumeric1.equals(alfanumeric2)) {
            return Encoder.sentenceEncoder(alfanumeric1, bsTree);
        }
        String biggestSequence = "";
        String alfaMorse1 = Encoder.sentenceEncoder(alfanumeric1, bsTree);
        String alfaMorse2 = Encoder.sentenceEncoder(alfanumeric2, bsTree);
        String smallest;
        if (alfaMorse1.length() < alfaMorse2.length()) {
            smallest = alfaMorse1;
        } else {
            smallest = alfaMorse2;
        }
        int i = 0;
        boolean finished = false;
        while (!finished && i < smallest.length()){
            if (alfaMorse1.charAt(i) != alfaMorse2.charAt(i)) {
                biggestSequence = alfaMorse1.substring(0, i);
                finished = true;
            }
            i++;
            if (i == smallest.length()){
                biggestSequence += smallest;
            }
        }
        if (biggestSequence.length() > 0) {
            if (biggestSequence.charAt(biggestSequence.length() - 1) == '/' || biggestSequence.charAt(biggestSequence.length() - 1) == ' ') {
                return biggestSequence.substring(0, biggestSequence.length() - 1);
            }
        }
        return biggestSequence;
    }

}
