package Utils;

import BST_Package.BST_Extended;

public class Encoder {

    private static final String WORD_DIVIDER = "/";
    private static final String WORD_SEPARATOR = " ";
    private static final String CHAR_DIVIDER = " ";

    public static String sentenceEncoder(String sentence, BST_Extended bsTree) {
        String encodedWord = "";
        for (String word : sentence.trim().split(WORD_SEPARATOR)) {
            for (int i = 0; i < word.length(); i++) {
                encodedWord += bsTree.encryptLetter(String.valueOf(word.charAt(i))) + CHAR_DIVIDER;
            }
            if (sentence.trim().split(WORD_SEPARATOR).length > 1){
                encodedWord += WORD_DIVIDER;
            }
        }
        return encodedWord.substring(0, encodedWord.length() - 1);
    }
}
