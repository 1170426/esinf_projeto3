package Utils;

import BST_Package.BST_Extended;
import Model.morseCharacter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FileManager {
    
    public static boolean readFile(File morseFile, BST_Extended bsTree) throws IOException{
        List<String> morseList = getFileLines(morseFile.getAbsolutePath());
        morseCharacter start = new morseCharacter("start", "");
        bsTree.insert(start);
        for (int i = 0; i < morseList.size(); i++){
            String[] line = morseList.get(i).trim().split(" ");
            morseCharacter character = new morseCharacter(line[1], line[2]);
            bsTree.insert(character, line[0]);
        }
        return true;
    }
    
    private static List<String> getFileLines(String filePath) throws IOException {
        List<Object> listObject = Files.lines(Paths.get(filePath)).collect(Collectors.toList());
        List<String> listString = new ArrayList<>();
        for (Object obj : listObject) {
            listString.add((String) obj);
        }
        return listString;
    }
    
}
