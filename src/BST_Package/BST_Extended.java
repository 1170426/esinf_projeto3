package BST_Package;

import Model.morseCharacter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BST_Extended extends BST {

    int i = -1;

    public void insert(morseCharacter element, String codeMorse) {
        i = -1;
        root = insert(element, root, codeMorse);
    }

    private Node insert(morseCharacter element, Node<morseCharacter> node, String codeMorse) {
        if (node == null) {
            return new Node(element, null, null);
        }
        if (node.getElement() == element) {
            node.setElement(element);
        } else {
            i++;
            if (codeMorse.charAt(i) == '.') {
                node.setLeft(insert(element, node.getLeft(), codeMorse));
            } else {
                node.setRight(insert(element, node.getRight(), codeMorse));
            }
        }
        return node;
    }

    public morseCharacter findElement(String morseWord) {
        i = -1;
        return (morseCharacter) findElement(morseWord, root).getElement();
    }

    private Node findElement(String morseWord, Node<morseCharacter> node) {
        if (node == null) {
            return null;
        }
        i++;
        if (morseWord.length() != i) {
            if (morseWord.charAt(i) == '.') {
                return findElement(morseWord, node.getLeft());
            } else {
                return findElement(morseWord, node.getRight());
            }
        } else {
            return node;
        }
    }

    public BST_Extended copy(BST_Extended letterTree) {
        letterTree.root = copy(this.root, letterTree.root);
        return letterTree;
    }

    private Node copy(Node bst1, Node bst2) {
        if (bst1 != null) {
            bst2 = new Node(bst1.getElement(), null, null);
            if (bst1.getLeft() != null) {
                if (((morseCharacter) bst1.getLeft().getElement()).getType().equals("Letter")) {
                    bst2.setLeft(copy(bst1.getLeft(), bst2.getLeft()));
                }
            }
            if (bst1.getRight() != null) {
                if (((morseCharacter) bst1.getRight().getElement()).getType().equals("Letter")) {
                    bst2.setRight(copy(bst1.getRight(), bst2.getRight()));
                }
            }
            return bst2;
        }
        return null;
    }

    public String encryptLetter(String letter) {
        List<String> path = new ArrayList<>(Arrays.asList(""));
        StringBuilder way = new StringBuilder();
        encryptLetter(root, letter, path, way);
        return way.toString();
    }

    private void encryptLetter(Node<morseCharacter> node, String letter, List<String> path, StringBuilder way) {
        if (node == null) {
            path.add(path.get(0).substring(0, path.get(0).length() - 1));
            path.remove(0);
            return;
        }
        if (node.getElement().getAlfanumeric().equalsIgnoreCase(letter)){
            way.append(path.get(0));
        }
        path.add(path.get(0) + ".");
        path.remove(0);
        encryptLetter(node.getLeft(), letter, path, way);
        path.add(path.get(0) + "_");
        path.remove(0);
        encryptLetter(node.getRight(), letter, path, way);
        if (path.get(0).length() != 0){
            path.add(path.get(0).substring(0, path.get(0).length() - 1));
            path.remove(0);
        }
    }
}
