package Model;

import java.util.Objects;

public class morseCharacter implements Comparable{
    
    private String alfanumeric;
    private String type;

    public morseCharacter(String alfanumeric, String type) {
        this.alfanumeric = alfanumeric;
        this.type = type;
    }

    public String getAlfanumeric() {
        return alfanumeric;
    }

    public String getType() {
        return type;
    }

    public void setAlfanumeric(String alfanumeric) {
        this.alfanumeric = alfanumeric;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    @Override
    public int compareTo(Object o) {
        return alfanumeric.compareTo(((morseCharacter) o).getAlfanumeric());
    }

    @Override
    public String toString() {
        return alfanumeric + " " + type;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final morseCharacter other = (morseCharacter) obj;
        if (!Objects.equals(this.alfanumeric, other.alfanumeric)) {
            return false;
        }
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        return true;
    }
    
    
    
}
