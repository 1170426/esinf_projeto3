package BST_Package;

import Model.morseCharacter;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class BST_ExtendedTest {
    
    private BST_Extended instance = new BST_Extended();
    
    public BST_ExtendedTest() {
    }
    
    @Before
    public void setUp() {
        morseCharacter element = new morseCharacter("E", "Letter");
        String codeMorse = ".";
        instance.insert(element, codeMorse);
    }
    
    @Test
    public void testInsert() {
        System.out.println("insert");
        morseCharacter element = new morseCharacter("T", "Letter");
        String codeMorse = "_";
        instance.insert(element, codeMorse);
        assertEquals(2, instance.size());
    }

    /**
     * Test of findElement method, of class BST_Extended.
     */
    @Test
    public void testFindElement() {
        System.out.println("findElement");
        morseCharacter expResult = new morseCharacter("T", "Letter");
        instance.insert(new morseCharacter("T", "Letter"), "_");
        morseCharacter result = instance.findElement("_");
        assertEquals(expResult, result);
    }
    
}
