package Utils;

import BST_Package.BST_Extended;
import Model.morseCharacter;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class OrderMorseWordsTest {
    
    private BST_Extended bsTree = new BST_Extended();
    
    public OrderMorseWordsTest() {
        bsTree.insert(new morseCharacter("start", ""), "");
        bsTree.insert(new morseCharacter("E", "Letter"), ".");
        bsTree.insert(new morseCharacter("T", "Letter"), "_");
        bsTree.insert(new morseCharacter("I", "Letter"), "..");
        bsTree.insert(new morseCharacter("A", "Letter"), "._");
        bsTree.insert(new morseCharacter("M", "Letter"), "__");
        bsTree.insert(new morseCharacter("N", "Letter"), "_.");
        bsTree.insert(new morseCharacter("1", "Number"), "_..");
        bsTree.insert(new morseCharacter("2", "Number"), "__.");
        bsTree.insert(new morseCharacter(",", "Punctuation"), "_._");
        bsTree.insert(new morseCharacter(".", "Punctuation"), "___");
        bsTree.insert(new morseCharacter("É", "Non-English"), "._.");
        bsTree.insert(new morseCharacter("Ŝ", "Non-English"), "...");
    }
    
    @Before
    public void setUp() {
        
    }
    
    @Test
    public void testOrderMorseWordsLetters() {
        System.out.println("orderMorseWordsLetters");
        List<String> morseWords = new ArrayList<>();
        morseWords.add("."); //E - 1 Letter
        morseWords.add("_ ./._ __"); //TE AM - 4 Letters
        morseWords.add("__ ../_.. __."); //MI 12 - 2 Letters
        morseWords.add("._. ... / _.. / _._ ___"); //ÉŜ 1 ,. - 0 Letters (does not appear)
        String type = "Letter";
        List<String> expResult = new ArrayList<>();
        expResult.add("TE AM");
        expResult.add("MI 12");
        expResult.add("E");
        List<String> result = OrderMorseWords.orderMorseWords(bsTree, morseWords, type);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testOrderMorseWordsNumbers() {
        System.out.println("orderMorseWordsNumbers");
        List<String> morseWords = new ArrayList<>();
        morseWords.add("."); //E - 1 Letter
        morseWords.add("_ ./._ __"); //TE AM - 4 Letters
        morseWords.add("__ ../_.. __."); //MI 12 - 2 Letters
        morseWords.add("._. .../_../_._ ___"); //ÉŜ 1 ,. - 0 Letters (does not appear)
        String type = "Number";
        List<String> expResult = new ArrayList<>();
        expResult.add("MI 12");
        expResult.add("ÉŜ 1 ,.");
        List<String> result = OrderMorseWords.orderMorseWords(bsTree, morseWords, type);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testOrderMorseWordsPunctuation() {
        System.out.println("orderMorseWordsPunctuation");
        List<String> morseWords = new ArrayList<>();
        morseWords.add("."); //E - 1 Letter
        morseWords.add("_ ./._ __"); //TE AM - 4 Letters
        morseWords.add("__ ../_.. __."); //MI 12 - 2 Letters
        morseWords.add("._. ... / _.. /_._ ___"); //ÉŜ 1 ,. - 0 Letters (does not appear)
        String type = "Punctuation";
        List<String> expResult = new ArrayList<>();
        expResult.add("ÉŜ 1 ,.");
        List<String> result = OrderMorseWords.orderMorseWords(bsTree, morseWords, type);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testOrderMorseWordsNonEnglish() {
        System.out.println("orderMorseWordsNonEnglish");
        List<String> morseWords = new ArrayList<>();
        morseWords.add("."); //E - 1 Letter
        morseWords.add("_ ./._ __"); //TE AM - 4 Letters
        morseWords.add("__ ../_.. __."); //MI 12 - 2 Letters
        morseWords.add("._. ... / _.. /_._ ___"); //ÉŜ 1 ,. - 0 Letters (does not appear)
        String type = "Non-English";
        List<String> expResult = new ArrayList<>();
        expResult.add("ÉŜ 1 ,.");
        List<String> result = OrderMorseWords.orderMorseWords(bsTree, morseWords, type);
        assertEquals(expResult, result);
    }
    
}
