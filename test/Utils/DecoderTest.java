package Utils;

import BST_Package.BST_Extended;
import Model.morseCharacter;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
public class DecoderTest {
    
    private BST_Extended bsTree = new BST_Extended();
    
    public DecoderTest() {
    }
        
    @Before
    public void setUp() {
        bsTree.insert(new morseCharacter("start", ""), "");
        bsTree.insert(new morseCharacter("E", "Letter"), ".");
        bsTree.insert(new morseCharacter("T", "Letter"), "_");
        bsTree.insert(new morseCharacter("I", "Letter"), "..");
        bsTree.insert(new morseCharacter("A", "Letter"), "._");
        bsTree.insert(new morseCharacter("M", "Letter"), "__");
        bsTree.insert(new morseCharacter("N", "Letter"), "_.");
        bsTree.insert(new morseCharacter("1", "Number"), "_..");
        bsTree.insert(new morseCharacter("2", "Number"), "__.");
        bsTree.insert(new morseCharacter(",", "Punctuation"), "_._");
        bsTree.insert(new morseCharacter(".", "Punctuation"), "___");
        bsTree.insert(new morseCharacter("É", "Non-English"), "._.");
        bsTree.insert(new morseCharacter("Ŝ", "Non-English"), "...");
    }
    
    @Test
    public void testDecodeMorseCode() {
        System.out.println("decodeMorseCode");
        String morse = "_ ./._ __";
        String expResult = "TE AM";
        String result = Decoder.decodeMorseCode(bsTree, morse);
        assertEquals(expResult, result);
    }
    
}
